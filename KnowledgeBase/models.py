from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.

class Plataforma(models.Model):
    nombre = models.CharField(max_length=25, null=False)
    tipo_choice = [
    ('firewall', 'Firewall'),
    ('ips', 'IPS'),
    ('proxy', 'Proxy'),
    ('otro', 'Otro'),
    ]
    tipo = models.CharField(
        max_length=8,
        choices=tipo_choice,
        default='',
        null=False,
    )
    descripcion = models.CharField(max_length=100, null=False)
    imagen = models.ImageField(upload_to="plataforms/images/",null=True)

    def __str__(self):
        return self.nombre


class Comando(models.Model):
    plataforma = models.ForeignKey(Plataforma,related_name='comando_plataforma', on_delete=models.CASCADE, null=True)
    comando = models.CharField(max_length=30, null=False)
    descripcion = models.CharField(max_length=100, null=False)

    def __str__(self):
        return self.comando
    

class Cliente(models.Model):
    
    nombre = models.CharField(max_length=30, null=False)
    dias_choice = [
    ('ll', 'Lunes a Lunes'),
    ('lv', 'Lunes a Viernes'),
    ('lmv', 'Lunes-Miercoles-Viernes')
    ]
    dias =  models.CharField(max_length=4,choices=dias_choice,default='',null=False,)
    horarioInicio = models.TimeField(default=None)
    horarioFinal = models.TimeField(default=None)
    plataforma = models.ManyToManyField(Plataforma)
    imagen = models.ImageField(upload_to="clients/images/",null=True)

    def __str__(self):
        return self.nombre
    

class Contacto(models.Model):
    cliente = models.ForeignKey(Cliente,related_name='contacto_cliente', on_delete=models.CASCADE, null=True)
    nombre = models.CharField(max_length=25, null=False)
    email = models.EmailField()
    celular = models.SmallIntegerField()
    fijo = models.CharField(max_length=20)

    def __str__(self):
        return "{0} ({1})".format(self.nombre, self.cliente)
    
class Actividad(models.Model):
    cliente = models.ForeignKey(Cliente,related_name='actividad_cliente', on_delete=models.CASCADE, null=True)
    titulo = models.CharField(max_length=25, null=False)
    fecha = models.DateField(null=False)
    hora = models.TimeField(null=False, default=None)
    descripcion = models.CharField(max_length=500, null=False)

    def __str__(self):
        return "{0} ({1})".format(self.titulo, self.cliente)
    

class Documento(models.Model):
    titulo = models.CharField(max_length=25, null=False)
    fechaPublicacion = models.DateField(null=False, default=timezone.now)
    autor = models.ForeignKey(User,related_name='documento_cliente', on_delete=models.CASCADE, null=True)

    categoria_choice = [
    ('reportes', 'Reportes'),
    ('informes', 'Informes'),
    ('procesosBK', 'Proceso BackUp'),
    ('tutorial', 'Tutorial'),
    ('articulo', 'Artículo'),
    ]
    categoria = models.CharField(max_length=12,choices=categoria_choice,default='',null=False,)
    descripcion = models.CharField(max_length=200, default='')
    archivo = models.FileField(upload_to="documents/",)
    estado = models.BooleanField(default=False)
    
    def __str__(self):
        return self.titulo