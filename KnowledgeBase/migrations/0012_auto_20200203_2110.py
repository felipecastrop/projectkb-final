# Generated by Django 3.0.2 on 2020-02-04 02:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('KnowledgeBase', '0011_auto_20200203_2002'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='plataforma',
            name='imagen',
        ),
        migrations.AddField(
            model_name='plataforma',
            name='image',
            field=models.ImageField(null=True, upload_to='images/plataforms/'),
        ),
    ]
