# Generated by Django 3.0.2 on 2020-02-04 00:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('KnowledgeBase', '0002_actividad_documento'),
    ]

    operations = [
        migrations.AddField(
            model_name='plataforma',
            name='imagenPath',
            field=models.FilePathField(null=True, path='static\\img\\clients'),
        ),
    ]
