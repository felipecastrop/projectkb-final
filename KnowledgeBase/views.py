from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.urls import reverse
from django.db.models import Q
from .models import *
from datetime import datetime
import re

# Create your views here.
def indexUser(request):
    plataformas = Plataforma.objects.all()
    contexto = {'plataformas_todas': plataformas}
    return render(request, 'index.html', contexto)

def error404(request):
    plataformas = Plataforma.objects.all()
    contexto = {'plataformas_todas': plataformas}
    return render(request, '404.html', contexto)

@staff_member_required
def indexAdmin(request):
    return render(request, 'index-admin.html')

def loginForm(request):
    return render(request, 'login.html')

def makeLogin(request):
    # Obtiene los datos de autenticacion
    username = request.POST['username']
    password = request.POST['password']

    # Obtiene el usuario con los datos de autenticacion
    user = authenticate(username=username, password=password)

    if user is not None:
        login(request, user)
        if user.is_staff:
            return HttpResponseRedirect(reverse('home-admin'))
        else:
            return HttpResponseRedirect(reverse('home'))
    else:
        contexto = {
            'username': username,
            'error': 'Usuario o contraseña incorrectos'
        }
        return render(request, 'login.html', contexto)

@login_required
def makeLogout(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))

@login_required
def changePass(request,id):
    usuario = User.objects.get(pk=id)

    if 'old-pass' in request.POST:
        if  usuario.check_password(request.POST['old-pass']):
            password = request.POST['new-pass']
            usuario.set_password(password)
            usuario.save()

            user = authenticate(username=usuario.username, password=password)
            login(request, user)
            contexto = {'msj': 'Tu contraseña ha sido cambiada con exito'}
        else:
            contexto ={'error': 'La contraseña antigua es incorrecta o no se digito'}

    return render(request,'index.html', contexto)


@login_required
def plataformList(request):
    plataformas = Plataforma.objects.all()
    contexto = {'plataformas_todas': plataformas}
    return render(request, 'plataform-list.html', contexto)

@login_required
def plataformView(request,tipo):
    plataforma = Plataforma.objects.filter(tipo=tipo)
    plataformas = Plataforma.objects.all()
    contexto = {
        'lista_plataforma': plataforma,
        'tipo': tipo,
        'plataformas_todas': plataformas
        }
    return render(request, 'plataform-view.html', contexto)

@login_required
def plataformKB(request,tipo,nombre):
    plataforma = Plataforma.objects.get(nombre=nombre)
    comandos = Comando.objects.filter(plataforma=plataforma.id)
    plataformas = Plataforma.objects.all()
    clientes = Cliente.objects.filter(plataforma=plataforma)
    contexto = {
        'plataforma': plataforma,
        'lista_comandos': comandos,
        'lista_clientes': clientes,
        'plataformas_todas': plataformas
    }
    return render(request, 'plataform-kb.html', contexto)

@login_required
def clientList(request):
    clientes = Cliente.objects.all()
    plataformas = Plataforma.objects.all()
    contexto = {
        'lista_clientes': clientes,
        'plataformas_todas': plataformas
    }
    return render(request, 'client-list.html', contexto)

@login_required
def clientView(request,id):
    cliente = Cliente.objects.get(pk=id)
    actividades = Actividad.objects.filter(cliente=cliente).order_by('-id')
    contactos = Contacto.objects.filter(cliente=cliente)
    plataformas = Plataforma.objects.all()
    contexto = {
        'cliente': cliente,
        'lista_actividades': actividades,
        'lista_contactos': contactos,
        'plataformas_todas': plataformas
    }
    return render(request, 'client-view.html', contexto)

@login_required
def documentList(request):
    documentos = Documento.objects.filter(estado=True).order_by('-fechaPublicacion')
    plataformas = Plataforma.objects.all()
    contexto = {
        'lista_documentos': documentos,
        'plataformas_todas': plataformas
    }
    return render(request, 'documentation.html', contexto)

@login_required
def documentView(request, id):
    documento = Documento.objects.get(pk=id)
    plataformas = Plataforma.objects.all()
    contexto = {
        'documento': documento,
        'plataformas_todas': plataformas
    }
    return render(request, 'documentation-view.html', contexto)

@login_required
def documentFilterCategory(request, categoria):
    documentos = Documento.objects.filter(categoria=categoria).order_by('-fechaPublicacion')
    plataformas = Plataforma.objects.all()
    contexto = {
        'lista_documentos': documentos,
        'categoria': categoria,
        'plataformas_todas': plataformas
    }
    return render(request, 'documentation-filter.html', contexto)

@login_required
def documentFilterAuthor(request, autor):
    userautor = User.objects.get(username=autor)
    documentos = Documento.objects.filter(autor=userautor).order_by('-fechaPublicacion')
    plataformas = Plataforma.objects.all()
    contexto = {
        'lista_documentos': documentos,
        'autor': autor,
        'plataformas_todas': plataformas
    }
    return render(request, 'documentation-filter.html', contexto)

@login_required
def documentFilterDate(request, fecha):
    documentos = Documento.objects.filter(fechaPublicacion=fecha).order_by('-fechaPublicacion')
    
    plataformas = Plataforma.objects.all()
    contexto = {
        'lista_documentos': documentos,
        'fecha': fecha,
        'plataformas_todas': plataformas
    }
    return render(request, 'documentation-filter.html', contexto)

@login_required
def sendDocumentForm(request):
    plataformas = Plataforma.objects.all()
    contexto = {
        'plataformas_todas': plataformas
    }
    return render(request, 'send-document.html', contexto)

@login_required
def sendDocument(request):
    titulo = request.POST['title']
    descripcion = request.POST['description']

    autor = request.user
    estado = False
    errors=[]
    
    if len(titulo) != 0:
        if titulo.isspace():
            errors.append('Título invalido')
        else:
           errors.append('')
    else:
        errors.append('Título invalido')

    if request.POST['category'] == '0':
        categoria = ''
        errors.append('Categoría invalida')        
    else:
        categoria = request.POST['category']
        errors.append('')

    if len(descripcion) != 0:
        if descripcion.isspace():
            errors.append('Descripcion invalida')
        else:
            errors.append('')
    else:
        errors.append('Descripcion invalida')

    if request.FILES:
        archivo = request.FILES['file']
        errors.append('')
    else:
        archivo = ''
        errors.append('Es necesario subir un archivo')

    print(categoria)
    print(errors)
    if errors.count('') < 4:
        plataformas = Plataforma.objects.all()
        contexto = {
            'plataformas_todas': plataformas,
            'titulo': titulo,
            'categoria': categoria,
            'descripcion': descripcion,
            'errors': errors
        }
        return render(request, 'send-document.html', contexto)
    else:
        documento = Documento(
            titulo=titulo,
            autor=autor,
            categoria=categoria,
            descripcion=descripcion,
            archivo=archivo,
            estado = estado
        )
        documento.save()
        contexto={'msj_doc': 'Se ha enviado el documento al administrador para revisión.'}

    return render(request, 'send-document.html', contexto)

#VIEWs ADMIN

#views DB User
@staff_member_required
def userList(request):
    queryset = request.GET.get('q',' ')
    busqueda = queryset.split(' ')
    usuarios = User.objects.all()
    if queryset:
        for palabra in busqueda:
            usuarios = User.objects.filter(
                Q(username__icontains = palabra) |
                Q(email__icontains = palabra) |
                Q(first_name__icontains = palabra) |
                Q(last_name__icontains = palabra)
            ).distinct()
    contexto = {'lista_usuarios': usuarios}
    return render(request, 'admin-users.html', contexto)

@staff_member_required
def userCreateForm(request):
    return render(request, 'admin-user-create.html')

@staff_member_required
def userCreate(request):
    username = request.POST['username']
    firstName = request.POST['firstName']
    lastName = request.POST['lastName']
    email = request.POST['email']
    password = request.POST['password']

    errors = []
    usr = User.objects.filter(username=username)

    if len(username) != 0:
        if username.isspace():
            errors.append('Username invalido')
        elif len(usr) != 0:
            errors.append('El username ya está siendo utilizado')
        else:
            errors.append('')
    else:
        errors.append('Username invalido')

    if len(firstName) != 0:
        if firstName.isspace():
            errors.append('Nombres invalidos')
        else:
           errors.append('')
    else:
        errors.append('Nombres invalidos')

    if len(lastName) != 0:
        if lastName.isspace():
            errors.append('Apellidos invalidos')
        else:
            errors.append('')
    else:
        errors.append('Apellidos invalidos')

    if len(email) != 0:
        if email.isspace():
            errors.append('Email invalido')
        elif re.match('^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,15}$',email.lower()):
            errors.append('')
        else:
            errors.append('Email invalido')
    else:
        errors.append('Email invalido')

                                        
    if len(password) != 0:
        if password.isspace():
            errors.append('Password invalido')
        else:
            errors.append('')
    else:
        errors.append('Password invalido')

    if 'is_admin' in request.POST:
        is_staff = True
    else:
        is_staff = False

    if errors.count('') < 5:
        contexto = {
            'errors': errors,
            'username': username,
            'firstName': firstName,
            'lastName': lastName,
            'email': email,
            'password': password
        }
        return render(request, 'admin-user-create.html', contexto)
    else:
        user = User(
            username=username,
            first_name=firstName,
            last_name=lastName,
            email=email,
            is_staff = is_staff
        )
        user.set_password(password)
        user.save()                                                              
                
    return HttpResponseRedirect(reverse('user-list'))

@staff_member_required
def userModForm(request, id):
    usuario = User.objects.get(pk=id)
    contexto = {
        'usuario': usuario
    }
    return render(request, 'admin-user-modify.html', contexto)

@staff_member_required
def userMod(request, id):
    username = request.POST['username']
    firstName = request.POST['firstName']
    lastName = request.POST['lastName']
    email = request.POST['email']

    errors = []
    usuario = User.objects.get(pk=id)
    usr = User.objects.filter(username=username)

    if len(username) != 0:
        if username.isspace():
            errors.append('Username invalido')
        elif usuario.username != username:
            if len(usr) != 0:
                errors.append('El username ya está siendo utilizado')
        else:
            errors.append('')
    else:
        errors.append('Username invalido')

    if len(firstName) != 0:
        if firstName.isspace():
            errors.append('Nombres invalidos')
        else:
           errors.append('')
    else:
        errors.append('Nombres invalidos')

    if len(lastName) != 0:
        if lastName.isspace():
            errors.append('Apellidos invalidos')
        else:
            errors.append('')
    else:
        errors.append('Apellidos invalidos')

    if len(email) != 0:
        if email.isspace():
            errors.append('Email invalido')
        elif re.match('^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,15}$',email.lower()):
            errors.append('')
        else:
            errors.append('Email invalido')
    else:
        errors.append('Email invalido')

    if errors.count('') < 4:
        contexto = {
            'errors': errors,
            'username': username,
            'firstName': firstName,
            'lastName': lastName,
            'email': email,
            'usuario': usuario            
        }
        return render(request, 'admin-user-modify.html', contexto)
    else:
        usuario = User.objects.get(pk=id)
        usuario.username = username
        usuario.first_name = firstName
        usuario.last_name = lastName
        usuario.email = email
    
        if 'is_admin' in request.POST:
            usuario.is_staff = True
        else:
            usuario.is_staff = False

        usuario.save() 
    
    return HttpResponseRedirect(reverse('user-list'))

@staff_member_required
def userDelete(request, id):
    usuario = User.objects.get(pk=id)
    usuario.delete()
    return HttpResponseRedirect(reverse('user-list'))

#views DB Actividad
@staff_member_required
def activityList(request):
    actividades = Actividad.objects.all()
    contexto = {'lista_actividades': actividades}
    return render(request, 'admin-activities.html', contexto)

@staff_member_required
def activityCreateForm(request):
    clientes = Cliente.objects.all()
    contexto = {'lista_clientes': clientes}
    return render(request, 'admin-activity-create.html',contexto)

@staff_member_required
def activityCreate(request):
    id_cliente = request.POST['id-client']
    titulo = request.POST['title']
    fecha = request.POST['date']
    hora = request.POST['hour']
    descripcion = request.POST['description']

    errors=[]  

    if id_cliente == '0':
        errors.append('Cliente invalido')
        cliente = ''
    else:
        cliente = Cliente.objects.get(id=id_cliente)
        errors.append('')    

    if len(titulo) != 0:
        if titulo.isspace():
            errors.append('Título invalido')
        else:
           errors.append('')
    else:
        errors.append('Título invalido')

    if len(fecha) != 0:
        if fecha.isspace():
            errors.append('Fecha invalida')
        else:
            errors.append('')
    else:
        errors.append('Fecha invalida')

    if len(hora) != 0:
        if hora.isspace():
            errors.append('Hora invalida')
        else:
            errors.append('')
    else:
        errors.append('Hora invalida')
                                        
    if len(descripcion) != 0:
        if descripcion.isspace():
            errors.append('Descripcion invalida')
        else:
            errors.append('')
    else:
        errors.append('Descripcion invalida')

    if errors.count('') < 5:
        clientes = Cliente.objects.all()
        contexto = {
            'lista_clientes': clientes,
            'cliente':cliente,
            'errors': errors,
            'titulo': titulo,
            'fecha': fecha,
            'hora': hora,
            'descripcion': descripcion
        }
        return render(request, 'admin-activity-create.html', contexto)
    else:
        actividad = Actividad(
            cliente=cliente,
            titulo=titulo,
            fecha=fecha,
            hora = hora,
            descripcion=descripcion,
        )
        actividad.save()

    return HttpResponseRedirect(reverse('activity-list'))

@staff_member_required
def activityModForm(request, id):
    actividad = Actividad.objects.get(pk=id)
    clientes = Cliente.objects.all()
    contexto = {
        'actividad': actividad,
        'lista_clientes': clientes
    }
    return render(request, 'admin-activity-modify.html', contexto)

@staff_member_required
def activityMod(request, id):
    titulo = request.POST['title']
    fecha = request.POST['date']
    hora = request.POST['hour']
    descripcion = request.POST['description']

    actividad = Actividad.objects.get(pk=id)

    if 'id-client' in request.POST:
        cliente = Cliente.objects.get(id=request.POST['id-client'])
    else:
        cliente = actividad.cliente 
    
    errors=[]   

    if len(titulo) != 0:
        if titulo.isspace():
            errors.append('Título invalido')
        else:
           errors.append('')
    else:
        errors.append('Título invalido')

    if len(fecha) != 0:
        if fecha.isspace():
            errors.append('Fecha invalida')
        else:
            errors.append('')
    else:
        errors.append('Fecha invalida')

    if len(hora) != 0:
        if hora.isspace():
            errors.append('Hora invalida')
        else:
            errors.append('')
    else:
        errors.append('Hora invalida')
                                        
    if len(descripcion) != 0:
        if descripcion.isspace():
            errors.append('Descripcion invalida')
        else:
            errors.append('')
    else:
        errors.append('Descripcion invalida')

    if errors.count('') < 4:
        clientes = Cliente.objects.all()
        contexto = {
            'lista_clientes': clientes,
            'actividad': actividad,
            'errors': errors,
            'titulo': titulo,
            'fecha': fecha,
            'hora': hora,
            'descripcion': descripcion
        }
        return render(request, 'admin-activity-modify.html', contexto)
    else:
        actividad.cliente = cliente
        actividad.titulo = titulo
        actividad.fecha = fecha
        actividad.hora = hora
        actividad.descripcion = descripcion

        actividad.save()

    return HttpResponseRedirect(reverse('activity-list'))

@staff_member_required
def activityDelete(request, id):
    actividad = Actividad.objects.get(pk=id)
    actividad.delete()
    return HttpResponseRedirect(reverse('activity-list'))

#views DB Cliente
@staff_member_required
def adminClientList(request):
    clientes = Cliente.objects.all()
    contexto = {'lista_clientes': clientes}
    return render(request, 'admin-clients.html', contexto)

@staff_member_required
def clientCreateForm(request):
    plataformas = Plataforma.objects.all()
    contexto = {
        'lista_plataformas': plataformas}
    return render(request, 'admin-client-create.html', contexto)

@staff_member_required
def clientCreate(request):
    nombre = request.POST['name']
    dias = request.POST['days']
    horarioInicio = request.POST['hour1']
    horarioFinal = request.POST['hour2']
    id_plataformas = request.POST.getlist('plataform')

    errors=[]

    if len(nombre) != 0:
        if nombre.isspace():
            errors.append('Nombre invalido')
        else:
           errors.append('')
    else:
        errors.append('Nombre invalido')

    if len(horarioInicio) != 0:
        if horarioInicio.isspace():
            errors.append('Inicio horario invalido')
        else:
            errors.append('')
    else:
        errors.append('Inicio horario invalido')

    if len(horarioFinal) != 0:
        if horarioFinal.isspace():
            errors.append('Final horario invalido')
        else:
            errors.append('')
    else:
        errors.append('Final horario invalido')

    if len(id_plataformas) == 0:
        errors.append('Es necesario seleccionar mínimo una plataforma')
    else:
        errors.append('')

    if request.FILES:
        imagen = request.FILES['image']
        errors.append('')
    else:
        imagen = ''
        errors.append('Es necesario subir una imagen')

    if errors.count('') < 5:
        plataformas = Plataforma.objects.all()
        contexto = {
            'lista_plataformas': plataformas,
            'nombre':nombre,
            'horarioInicio': horarioInicio,
            'horarioFinal': horarioFinal,
            'errors': errors
        }
        return render(request, 'admin-client-create.html', contexto)
    else:
        cliente = Cliente(
            nombre=nombre,
            dias = dias,
            horarioInicio=horarioInicio,
            horarioFinal=horarioFinal,
            imagen=imagen,
        )
        cliente.save()
        cliente.plataforma.add(*id_plataformas)

    return HttpResponseRedirect(reverse('admin-client-list'))

@staff_member_required
def clientModForm(request, id):
    cliente = Cliente.objects.get(pk=id)
    plataformas = Plataforma.objects.all()
    contexto = {
        'cliente': cliente,
        'lista_plataformas': plataformas
    }
    return render(request, 'admin-client-modify.html', contexto)

@staff_member_required
def clientMod(request, id):
    nombre = request.POST['name']
    dias = request.POST['days']
    horarioInicio = request.POST['hour1']
    horarioFinal = request.POST['hour2']
    id_plataformas = request.POST.getlist('plataform')
    print(id_plataformas)
    cliente = Cliente.objects.get(pk=id)
    errors=[]

    if len(nombre) != 0:
        if nombre.isspace():
            errors.append('Nombre invalido')
        else:
           errors.append('')
    else:
        errors.append('Nombre invalido')

    if len(horarioInicio) != 0:
        if horarioInicio.isspace():
            errors.append('Inicio horario invalido')
        else:
            errors.append('')
    else:
        errors.append('Inicio horario invalido')

    if len(horarioFinal) != 0:
        if horarioFinal.isspace():
            errors.append('Final horario invalido')
        else:
            errors.append('')
    else:
        errors.append('Final horario invalido')

    if len(id_plataformas) == 0:
        errors.append('Es necesario seleccionar mínimo una plataforma')
    else:
        errors.append('')

    if request.FILES:
        imagen = request.FILES['image']
        cliente.imagen=imagen

    if errors.count('') < 4:
        clientes = Cliente.objects.all()
        plataformas = Plataforma.objects.all()
        contexto = {
            'cliente': cliente,
            'lista_plataformas': plataformas,
            'nombre':nombre,
            'horarioInicio': horarioInicio,
            'horarioFinal': horarioFinal,
            'errors': errors
        }
        return render(request, 'admin-client-modify.html', contexto)
    else:
        cliente.nombre = nombre
        cliente.dias = dias
        cliente.horarioInicio = horarioInicio
        cliente.horarioFinal = horarioFinal
        cliente.plataforma.clear()
        cliente.plataforma.add(*id_plataformas)
        cliente.save()

    return HttpResponseRedirect(reverse('admin-client-list'))

@staff_member_required
def clientDelete(request, id):
    cliente = Cliente.objects.get(pk=id)
    cliente.delete()
    return HttpResponseRedirect(reverse('admin-client-list'))

#views DB Comando
@staff_member_required
def commandList(request):
    comandos = Comando.objects.all()
    contexto = {'lista_comandos': comandos}
    return render(request, 'admin-commands.html', contexto)

@staff_member_required
def commandCreateForm(request):
    plataforma = Plataforma.objects.all()
    contexto = {'lista_plataformas': plataforma}
    return render(request, 'admin-command-create.html', contexto)

@staff_member_required
def commandCreate(request):
    id_plataforma = request.POST['id-plataform']
    comando = request.POST['command']
    descripcion = request.POST['description']

    errors = []

    if id_plataforma == '0':
        errors.append('Plataforma invalida')
        plataforma = ''
    else:
        plataforma = Plataforma.objects.get(id=id_plataforma)
        errors.append('')    

    if len(comando) != 0:
        if comando.isspace():
            errors.append('Comando invalido')
        else:
           errors.append('')
    else:
        errors.append('Comando invalido')

    if len(descripcion) != 0:
        if descripcion.isspace():
            errors.append('Descripcion invalida')
        else:
            errors.append('')
    else:
        errors.append('Descripcion invalida')

    if errors.count('') < 3:
        plataformas = Plataforma.objects.all()
        contexto = {
            'lista_plataformas': plataformas,
            'plataforma':plataforma,
            'comando': comando,
            'descripcion': descripcion,
            'errors': errors
        }
        return render(request, 'admin-command-create.html', contexto)
    else:
        comando = Comando(
            plataforma=plataforma,
            comando=comando,
            descripcion=descripcion
        )
        comando.save()

    return HttpResponseRedirect(reverse('command-list'))

@staff_member_required
def commandModForm(request, id):
    comando = Comando.objects.get(pk=id)
    plataformas = Plataforma.objects.all()
    contexto = {
        'comando': comando,
        'lista_plataformas': plataformas
    }
    return render(request, 'admin-command-modify.html', contexto)

@staff_member_required
def commandMod(request, id):
    plataforma = Plataforma.objects.get(id=request.POST['id-plataform'])
    comand = request.POST['command']
    descripcion = request.POST['description']

    comando = Comando.objects.get(pk=id)
    errors=[]

    if len(comand) != 0:
        if comand.isspace():
            errors.append('Comando invalido')
        else:
           errors.append('')
    else:
        errors.append('Comando invalido')

    if len(descripcion) != 0:
        if descripcion.isspace():
            errors.append('Descripcion invalida')
        else:
            errors.append('')
    else:
        errors.append('Descripcion invalida')

    if errors.count('') < 2:
        plataformas = Plataforma.objects.all()
        contexto = {
            'lista_plataformas': plataformas,
            'comando': comando,
            'comand': comand,
            'descripcion': descripcion,
            'errors': errors
        }
        return render(request, 'admin-command-modify.html', contexto)
    else:
        comando.plataforma = plataforma
        comando.comando = comand
        comando.descripcion = descripcion

        comando.save()

    return HttpResponseRedirect(reverse('command-list'))

@staff_member_required
def commandDelete(request, id):
    comando = Comando.objects.get(pk=id)
    comando.delete()
    return HttpResponseRedirect(reverse('command-list'))

#views DB Contacto
@staff_member_required
def contactList(request):
    contactos = Contacto.objects.all()
    contexto = {'lista_contactos': contactos}
    return render(request, 'admin-contacts.html', contexto)

@staff_member_required
def contactCreateForm(request):
    clientes = Cliente.objects.all()
    contexto = {'lista_clientes': clientes}
    return render(request, 'admin-contact-create.html',contexto)

@staff_member_required
def contactCreate(request):
    id_cliente = request.POST['id-client']
    nombre = request.POST['name']
    email = request.POST['email']
    celular = request.POST['cellphone']
    fijo = request.POST['phone']

    errors=[]  

    if id_cliente == '0':
        cliente = ''
        errors.append('Cliente invalido')        
    else:
        cliente = Cliente.objects.get(id=id_cliente)
        errors.append('')    

    if len(nombre) != 0:
        if nombre.isspace():
            errors.append('Nombre invalido')
        else:
           errors.append('')
    else:
        errors.append('Nombre invalido')

    if len(email) != 0:
        if email.isspace():
            errors.append('Email invalido')
        elif re.match('^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,15}$',email.lower()):
            errors.append('')
        else:
            errors.append('Email invalido')

    if len(celular) != 0:
        if celular.isspace():
            errors.append('Celular invalido')
        elif len(celular) != 11:
            errors.append('Celular invalido')
        else:
            errors.append('')
    else:
        errors.append('Celular invalido')

    if len(fijo) != 0:
        if fijo.isspace():
            errors.append('Fijo invalido')
        else:
            errors.append('')
    else:
        errors.append('Fijo invalido')

    if errors.count('') < 5:
        clientes = Cliente.objects.all()
        contexto = {
            'lista_clientes': clientes,
            'cliente': cliente,
            'nombre': nombre,
            'email': email,
            'celular': celular,
            'fijo': fijo,
            'errors': errors
        }
        return render(request, 'admin-contact-create.html', contexto)
    else:
        contacto = Contacto(
            cliente=cliente,
            nombre=nombre,
            email=email,
            celular=celular,
            fijo=fijo
        )
        contacto.save()

    return HttpResponseRedirect(reverse('contact-list'))

@staff_member_required
def contactModForm(request, id):
    contacto = Contacto.objects.get(pk=id)
    clientes = Cliente.objects.all()
    contexto = {
        'contacto': contacto,
        'lista_clientes': clientes
    }
    return render(request, 'admin-contact-modify.html', contexto)

@staff_member_required
def contactMod(request, id):
            
    nombre = request.POST['name']
    email = request.POST['email']
    celular = request.POST['cellphone']
    fijo = request.POST['phone']

    contacto = Contacto.objects.get(pk=id)

    if 'id-cliente' in request.POST:
        cliente = Cliente.objects.get(id=request.POST['id-cliente'])
    else:
        cliente = contacto.cliente

    errors=[]

    if len(nombre) != 0:
        if nombre.isspace():
            errors.append('Nombre invalido')
        else:
           errors.append('')
    else:
        errors.append('Nombre invalido')

    if len(email) != 0:
        if email.isspace():
            errors.append('Email invalido')
        elif re.match('^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,15}$',email.lower()):
            errors.append('')
        else:
            errors.append('Email invalido')

    if len(celular) != 0:
        if celular.isspace():
            errors.append('Celular invalido')
        elif len(celular) != 11:
            errors.append('Celular invalido')
        else:
            errors.append('')
    else:
        errors.append('Celular invalido')

    if len(fijo) != 0:
        if fijo.isspace():
            errors.append('Fijo invalido')
        else:
            errors.append('')
    else:
        errors.append('Fijo invalido')

    if errors.count('') < 4:
        clientes = Cliente.objects.all()
        contexto = {
            'contacto': contacto,
            'lista_clientes': clientes,
            'nombre': nombre,
            'email': email,
            'celular': celular,
            'fijo': fijo,
            'errors': errors
        }
        return render(request, 'admin-contact-modify.html', contexto)
    else:
        contacto.cliente=cliente
        contacto.nombre=nombre
        contacto.email=email
        contacto.celular=celular
        contacto.fijo=fijo
        
        contacto.save()
    return HttpResponseRedirect(reverse('contact-list'))

@staff_member_required
def contactDelete(request, id):
    contacto = Contacto.objects.get(pk=id)
    contacto.delete()
    return HttpResponseRedirect(reverse('contact-list'))

#views DB Documento
@staff_member_required
def adminDocumentList(request):
    documentos = Documento.objects.all()
    contexto = {'lista_documentos': documentos}
    return render(request, 'admin-documents.html', contexto)

@staff_member_required
def documentCreateForm(request):
    usuarios = User.objects.all()
    contexto = {'lista_usuarios': usuarios}
    return render(request, 'admin-document-create.html', contexto)

@staff_member_required
def documentCreate(request):
    titulo = request.POST['title']
    id_usuario = request.POST['id-user']
    categoria = request.POST['category']
    descripcion = request.POST['description']
    
    errors=[]
    
    if len(titulo) != 0:
        if titulo.isspace():
            errors.append('Título invalido')
        else:
           errors.append('')
    else:
        errors.append('Título invalido')

    if id_usuario == '0':
        autor = ''
        errors.append('Usuario invalido')
    else:
        autor = User.objects.get(pk=id_usuario)
        errors.append('')    
                                        
    if len(descripcion) != 0:
        if descripcion.isspace():
            errors.append('Descripcion invalida')
        else:
            errors.append('')
    else:
        errors.append('Descripcion invalida')

    if request.FILES:
        archivo = request.FILES['file']
        errors.append('')
    else:
        archivo = ''
        errors.append('Es necesario subir un archivo')

    if 'status' in request.POST:
        estado = True
    else:
        estado = False

    print(errors)
    if errors.count('') < 4:
        usuarios = User.objects.all()
        contexto = {
            'lista_usuarios': usuarios,
            'titulo': titulo,
            'autor': autor,
            'descripcion': descripcion,
            'errors': errors
        }
        return render(request, 'admin-document-create.html', contexto)
    else:
        documento = Documento(
            titulo=titulo,
            autor=autor,
            categoria=categoria,
            descripcion=descripcion,
            archivo=archivo,
            estado = estado
        )
        documento.save()

    return HttpResponseRedirect(reverse('admin-document-list'))

@staff_member_required
def documentModForm(request, id):
    documento = Documento.objects.get(pk=id)
    usuarios = User.objects.all()
    contexto = {
        'documento': documento,
        'lista_usuarios': usuarios
    }
    return render(request, 'admin-document-modify.html', contexto)

@staff_member_required
def documentMod(request, id):
    titulo = request.POST['title']
    categoria = request.POST['category']
    descripcion = request.POST['description']

    documento = Documento.objects.get(pk=id)
    errors=[]

    if 'id-user' in request.POST:
        autor = User.objects.get(pk=request.POST['id-user'])
    else:
        autor = documento.autor 

    print(autor)
    if len(titulo) != 0:
        if titulo.isspace():
            errors.append('Título invalido')
        else:
           errors.append('')
    else:
        errors.append('Título invalido')

    if len(descripcion) != 0:
        if descripcion.isspace():
            errors.append('Descripcion invalida')
        else:
            errors.append('')
    else:
        errors.append('Descripcion invalida')

    if request.FILES:
        archivo = request.FILES['file']
        documento.archivo=archivo        

    if 'status' in request.POST:
        documento.estado = True
    else:
        documento.estado = False

    if errors.count('') < 2:
        usuarios = User.objects.all()
        contexto = {
            'documento': documento,
            'lista_usuarios': usuarios,
            'titulo': titulo,
            'descripcion': descripcion,
            'errors': errors
        }
        return render(request, 'admin-document-modify.html', contexto)
    else:
        documento.titulo = titulo
        documento.autor = autor
        documento.categoria = categoria
        documento.descripcion = descripcion

        documento.save()

    return HttpResponseRedirect(reverse('admin-document-list'))

@staff_member_required
def documentDelete(request, id):
    documento = Documento.objects.get(pk=id)
    documento.delete()
    return HttpResponseRedirect(reverse('admin-document-list'))


#views DB Plataforma
@staff_member_required
def adminPlataformList(request):
    plataformas = Plataforma.objects.all()
    contexto = {'lista_plataformas': plataformas}
    return render(request, 'admin-plataforms.html', contexto)

@staff_member_required
def plataformCreateForm(request):
    return render(request, 'admin-plataform-create.html')

@staff_member_required
def plataformCreate(request):
    nombre = request.POST['name']
    tipo = request.POST['type']
    descripcion = request.POST['description']

    errors=[]

    if len(nombre) != 0:
        if nombre.isspace():
            errors.append('Nombre invalido')
        else:
           errors.append('')
    else:
        errors.append('Nombre invalido')

    if len(descripcion) != 0:
        if descripcion.isspace():
            errors.append('Descripcion invalida')
        else:
            errors.append('')
    else:
        errors.append('Descripcion invalida')
                                        
    if request.FILES:
        imagen = request.FILES['image']
        errors.append('')
    else:
        imagen = ''
        errors.append('Es necesario subir una imagen')

    if errors.count('') < 3:
        contexto = {
            'nombre': nombre,
            'descripcion': descripcion,
            'errors': errors
        }
        return render(request, 'admin-plataform-create.html', contexto)
    else:
        plataforma = Plataforma(
            nombre=nombre,
            tipo=tipo,
            descripcion=descripcion,
            imagen=imagen,
        )
        plataforma.save()

    return HttpResponseRedirect(reverse('admin-plataform-list'))

@staff_member_required
def plataformModForm(request, id):
    plataforma = Plataforma.objects.get(pk=id)
    contexto = {
        'plataforma': plataforma
    }
    return render(request, 'admin-plataform-modify.html', contexto)

@staff_member_required
def plataformMod(request, id):
    nombre = request.POST['name']
    tipo = request.POST['type']
    descripcion = request.POST['description']

    plataforma = Plataforma.objects.get(pk=id)
    errors=[]

    if request.FILES:
        imagen = request.FILES['image']
        plataforma.imagen=imagen

    if len(nombre) != 0:
        if nombre.isspace():
            errors.append('Nombre invalido')
        else:
           errors.append('')
    else:
        errors.append('Nombre invalido')

    if len(descripcion) != 0:
        if descripcion.isspace():
            errors.append('Descripcion invalida')
        else:
            errors.append('')
    else:
        errors.append('Descripcion invalida')

    if errors.count('') < 2:
        contexto = {
            'plataforma': plataforma,
            'nombre': nombre,
            'descripcion': descripcion,
            'errors': errors
        }
        return render(request, 'admin-plataform-modify.html', contexto)
    else:
        plataforma.nombre=nombre
        plataforma.tipo=tipo
        plataforma.descripcion=descripcion
        plataforma.save()

    return HttpResponseRedirect(reverse('admin-plataform-list'))

@staff_member_required
def plataformDelete(request, id):
    plataforma = Plataforma.objects.get(pk=id)
    plataforma.delete()
    return HttpResponseRedirect(reverse('admin-plataform-list'))
