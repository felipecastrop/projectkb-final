from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [

#URLs USUARIOS
path('', views.indexUser, name='home'),
path('<int:id>/cambiar',views.changePass, name='change-pass'),
path('404/',views.error404, name='404'),

#-----LOGIN-----
path('login/', views.loginForm, name='login-form'),
path('login/?', views.makeLogin, name='make-login'),
path('logout/', views.makeLogout, name='make-logout'),


#-----Consultar plataformas-----
path('plataformas/', views.plataformList, name='plataform-list'),
path('plataformas/<str:tipo>', views.plataformView, name='plataform-view'),
path('plataformas/<str:tipo>/<str:nombre>', views.plataformKB, name='plataform-kb'),

#-----Consultar Clientes-----
path('clientes/', views.clientList, name='client-list'),
path('clientes/<int:id>', views.clientView, name='client-view'),

#-----Consultar Documentos-----
path('documentos/',views.documentList, name='document-list'),
path('documentos/<int:id>',views.documentView, name='document-view'),
path('documentos/categoria/<str:categoria>',views.documentFilterCategory, name='document-filter-category'),
path('documentos/autor/<str:autor>',views.documentFilterAuthor, name='document-filter-author'),
path('documentos/fecha/<str:fecha>',views.documentFilterDate, name='document-filter-date'),

#------Enviar Documento----------
path('enviar-documento/',views.sendDocumentForm, name='send-document-form'),
path('enviar-documento/?',views.sendDocument, name='send-document'),

#URLs ADMIN
path('administrador/',views.indexAdmin, name='home-admin'),

#-----DB User-----
path('administrador/usuarios/',views.userList, name='user-list'),
path('administrador/usuarios/agregar/',views.userCreateForm, name='user-create-form'),
path('administrador/usuarios/agregar/?',views.userCreate, name='user-create'),
path('administrador/usuarios/<int:id>/modificar',views.userModForm, name='user-mod-form'),
path('administrador/usuarios/<int:id>/modificar/?',views.userMod, name='user-mod'),
path('administrador/usuarios/<int:id>/borrar',views.userDelete, name='user-delete'),

#-----DB Actividad-----
path('administrador/actividades/',views.activityList, name='activity-list'),
path('administrador/actividades/agregar/',views.activityCreateForm, name='activity-create-form'),
path('administrador/actividades/agregar/?',views.activityCreate, name='activity-create'),
path('administrador/actividades/<int:id>/modificar',views.activityModForm, name='activity-mod-form'),
path('administrador/actividades/<int:id>/modificar/?',views.activityMod, name='activity-mod'),
path('administrador/actividades/<int:id>/borrar/',views.activityDelete, name='activity-delete'),

#-----DB Cliente-----
path('administrador/clientes/',views.adminClientList, name='admin-client-list'),
path('administrador/clientes/agregar/',views.clientCreateForm, name='client-create-form'),
path('administrador/clientes/agregar/?',views.clientCreate, name='client-create'),
path('administrador/clientes/<int:id>/modificar',views.clientModForm, name='client-mod-form'),
path('administrador/clientes/<int:id>/modificar/?',views.clientMod, name='client-mod'),
path('administrador/clientes/<int:id>/borrar/',views.clientDelete, name='client-delete'),

#-----DB Comando-----
path('administrador/comandos/',views.commandList, name='command-list'),
path('administrador/comandos/agregar/',views.commandCreateForm, name='command-create-form'),
path('administrador/comandos/agregar/?',views.commandCreate, name='command-create'),
path('administrador/comandos/<int:id>/modificar',views.commandModForm, name='command-mod-form'),
path('administrador/comandos/<int:id>/modificar/?',views.commandMod, name='command-mod'),
path('administrador/comandos/<int:id>/borrar/',views.commandDelete, name='command-delete'),

#-----DB Contacto-----
path('administrador/contactos/',views.contactList, name='contact-list'),
path('administrador/contactos/agregar/',views.contactCreateForm, name='contact-create-form'),
path('administrador/contactos/agregar/?',views.contactCreate, name='contact-create'),
path('administrador/contactos/<int:id>/modificar',views.contactModForm, name='contact-mod-form'),
path('administrador/contactos/<int:id>/modificar/?',views.contactMod, name='contact-mod'),
path('administrador/contactos/<int:id>/borrar/',views.contactDelete, name='contact-delete'),

#-----DB Documento-----
path('administrador/documentos/',views.adminDocumentList, name='admin-document-list'),
path('administrador/documentos/agregar/',views.documentCreateForm, name='document-create-form'),
path('administrador/documentos/agregar/?',views.documentCreate, name='document-create'),
path('administrador/documentos/<int:id>/modificar',views.documentModForm, name='document-mod-form'),
path('administrador/documentos/<int:id>/modificar/?',views.documentMod, name='document-mod'),
path('administrador/documentos/<int:id>/borrar/',views.documentDelete, name='document-delete'),

#-----DB Plataforma-----
path('administrador/plataformas/',views.adminPlataformList, name='admin-plataform-list'),
path('administrador/plataformas/agregar/',views.plataformCreateForm, name='plataform-create-form'),
path('administrador/plataformas/agregar/?',views.plataformCreate, name='plataform-create'),
path('administrador/plataformas/<int:id>/modificar',views.plataformModForm, name='plataform-mod-form'),
path('administrador/plataformas/<int:id>/modificar/?',views.plataformMod, name='plataform-mod'),
path('administrador/plataformas/<int:id>/borrar/',views.plataformDelete, name='plataform-delete'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)